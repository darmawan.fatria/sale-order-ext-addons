from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
from openerp import SUPERUSER_ID

class stock_picking(osv.osv):
    _inherit = 'stock.picking'

    def action_invoice_create(self, cr, uid, ids, journal_id, group=False, type='out_invoice', context=None):
        print "account invoice create"
        return super(stock_picking, self).action_invoice_create(cr, uid, ids, journal_id, group=False, type='out_invoice', context=context)

    def _get_invoice_vals(self, cr, uid, key, inv_type, journal_id, move, context=None):
        res = super(stock_picking, self)._get_invoice_vals(cr, uid, key, inv_type, journal_id, move,context=context)
        print "res : ",res
        if move and move.picking_id:
            sale_pool = self.pool.get('sale.order')
            search_criteria = [('name', '=', move.origin)]
            sale_ids = sale_pool.search(cr, uid, search_criteria, context=None)
            if sale_ids:
                for sale_order_obj in sale_pool.browse(cr, uid, sale_ids, context=None):
                    res['origin'] = sale_order_obj.name
                    res['amount_total_1'] = sale_order_obj.amount_total_1
                    res['amount_total_1_a'] = sale_order_obj.amount_total_1_a
                    res['amount_total_2'] = sale_order_obj.amount_total_2
                    res['amount_total_3'] = sale_order_obj.amount_total_3
                    res['discount_amount'] = sale_order_obj.discount_amount
                    res['discount_value'] = sale_order_obj.discount_value
                    res['public_pricelist_discount_amount'] = sale_order_obj.public_pricelist_discount_amount
                    res['order_line_discount_amount'] = sale_order_obj.order_line_discount_amount
        print res
        return res

    def _invoice_create_line(self, cr, uid, moves, journal_id, inv_type='out_invoice', context=None):
        print "_invoice_create_line "
        return super(stock_picking, self)._invoice_create_line(cr, uid, moves, journal_id, inv_type='out_invoice', context=context)

stock_picking()