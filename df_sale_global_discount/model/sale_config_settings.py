from openerp import api, models, fields
from openerp.osv import fields, osv
from openerp import SUPERUSER_ID


class sale_config_settings(osv.TransientModel):
    _inherit = 'sale.config.settings'
    _columns = {
        'global_amount_discount_limit': fields.float('Total Limit Amount for Global Discount', required=True,),
        'order_line_amount_discount_limit': fields.float('Total Limit Amount for Order Line', required=True, ),
        'discount_account_id': fields.many2one('account.account', 'Account for Discount', required=True, ),
    }

    _defaults = {
        'global_amount_discount_limit': 10,
    }

    def get_default_global_amount_discount_limit(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        global_amount_discount_limit = ir_values.get_default(cr, uid, 'sale.config.settings', 'global_amount_discount_limit')
        return {
            'global_amount_discount_limit': global_amount_discount_limit,
        }

    def set_global_amount_discount_limit(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        wizard = self.browse(cr, uid, ids)[0]
        if wizard.global_amount_discount_limit:
            global_amount_discount_limit = wizard.global_amount_discount_limit
            ir_values.set_default(cr, SUPERUSER_ID, 'sale.config.settings', 'global_amount_discount_limit', global_amount_discount_limit)

    def get_default_order_line_amount_discount_limit(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        order_line_amount_discount_limit = ir_values.get_default(cr, uid, 'sale.config.settings',
                                                             'order_line_amount_discount_limit')
        return {
            'order_line_amount_discount_limit': order_line_amount_discount_limit,
        }

    def set_order_line_amount_discount_limit(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        wizard = self.browse(cr, uid, ids)[0]
        if wizard.global_amount_discount_limit:
            order_line_amount_discount_limit = wizard.order_line_amount_discount_limit
            ir_values.set_default(cr, SUPERUSER_ID, 'sale.config.settings', 'order_line_amount_discount_limit',
                                  order_line_amount_discount_limit)

    def get_default_discount_account_id(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        discount_account_id = ir_values.get_default(cr, uid, 'sale.config.settings', 'discount_account_id')
        return {
            'discount_account_id': discount_account_id,
        }

    def set_discount_account_id(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        wizard = self.browse(cr, uid, ids)[0]
        if wizard.discount_account_id:
            discount_account_id = wizard.discount_account_id.id
            ir_values.set_default(cr, SUPERUSER_ID, 'sale.config.settings', 'discount_account_id',
                                  discount_account_id)
