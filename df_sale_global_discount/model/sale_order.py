from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
from openerp import SUPERUSER_ID

class sale_order(osv.osv):
    _inherit = 'sale.order'

    # def _get_discount_amount(self,cr,uid,ids,context=None):
    #     return

    def onchange_global_discount(self, cr, uid, ids, amount_untaxed,discount_type,discount_value):
        discount_amount = 0.0
        if discount_type == 'percent' and amount_untaxed:
            discount_amount = amount_untaxed * discount_value / 100
        else:
            discount_amount = discount_value

        return {'value': {'discount_amount': discount_amount}}

    def _amount_all_wrapper(self, cr, uid, ids, field_name, arg, context=None):
        return super(sale_order, self)._amount_all_wrapper( cr, uid, ids, field_name, arg, context=context)

    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('sale.order.line').browse(cr, uid, ids, context=context):
            result[line.order_id.id] = True
        return result.keys()

    _columns = {
        'total_order_line_discount_amount': fields.float('Max Amount For Order Line Discount'),
        'total_global_discount_amount': fields.float('Max Amount For Global Discount'),
        'discount_type': fields.selection(string="Discount Type",readonly=True, states={'draft': [('readonly', False)]} ,
                                          selection=[('percent','Percentage'),
                                                     ('amount','Amount')], ),
        'discount_value': fields.float('Discount Value',  readonly=True, states={'draft': [('readonly', False)]} ),
        # 'discount_amount_function': fields.function(_get_discount_amount, digits_compute=dp.get_precision('Account'),
        #                                    string='Global Discount Amount',store=False ,help="discount amount only view."),
        'discount_amount': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Global Discount Amount',
                            store={
                                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','discount_value','discount_type'], 10),
                                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
                            },
                            multi='sums', help="discount amount."),
        'amount_untaxed': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'),
                                          string='Untaxed Amount',
                                          store={
                                              'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','discount_value','discount_type'], 10),
                                              'sale.order.line': (
                                              _get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
                                          },
                                          multi='sums', help="The amount without tax.", track_visibility='always'),
        'amount_tax': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Taxes',
                                      store={
                                          'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','discount_value','discount_type'], 10),
                                          'sale.order.line': (
                                          _get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
                                      },
                                      multi='sums', help="The tax amount."),
        'amount_total': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Total',
                                        store={
                                            'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','discount_value','discount_type'], 10),
                                            'sale.order.line': (
                                            _get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
                                        },
                                        multi='sums', help="The total amount."),

        'public_pricelist_discount_amount': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'),
                                           string='Total Public Pricelist Discount',
                                           store={
                                               'sale.order': (lambda self, cr, uid, ids, c={}: ids,
                                                              ['order_line', 'discount_value', 'discount_type'], 10),
                                               'sale.order.line': (
                                               _get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
                                           },
                                           multi='sums', help="Discount Level 1."),
        'order_line_discount_amount': fields.function(_amount_all_wrapper,
                                                digits_compute=dp.get_precision('Account'),
                                                string='Total Discount Order Line',
                                                store={
                                                                'sale.order': (lambda self, cr, uid, ids, c={}: ids,
                                                                               ['order_line', 'discount_value',
                                                                                'discount_type'], 10),
                                                                'sale.order.line': (
                                                                    _get_order, ['price_unit', 'tax_id', 'discount',
                                                                                 'product_uom_qty'], 10),
                                                },
                                                multi='sums', help="Discount Level 2."),

        'amount_total_1': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'), string='Total Before Public Pricelist',
                                        store={
                                            'sale.order': (lambda self, cr, uid, ids, c={}: ids,
                                                           ['order_line', 'discount_value', 'discount_type'], 10),
                                            'sale.order.line': (
                                                _get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'],
                                                10),
                                        },
                                        multi='sums', help="The total amount before discount level 1."),
        'amount_total_1_a': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'),
                                          string='Total Unit Price Order Line',
                                          store={
                                              'sale.order': (lambda self, cr, uid, ids, c={}: ids,
                                                             ['order_line', 'discount_value', 'discount_type'], 10),
                                              'sale.order.line': (
                                                  _get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'],
                                                  10),
                                          },
                                          multi='sums', help="The total amount unit price before order line discount."),
        'amount_total_2': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'),
                                          string='Total After Public Order Line Discount',
                                          store={
                                              'sale.order': (lambda self, cr, uid, ids, c={}: ids,
                                                             ['order_line', 'discount_value', 'discount_type'], 10),
                                              'sale.order.line': (
                                                  _get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'],
                                                  10),
                                          },
                                          multi='sums', help="The total amount after discount level 2."),
        'amount_total_3': fields.function(_amount_all_wrapper, digits_compute=dp.get_precision('Account'),
                                          string='Total After Public Global Discount',
                                          store={
                                              'sale.order': (lambda self, cr, uid, ids, c={}: ids,
                                                             ['order_line', 'discount_value', 'discount_type'], 10),
                                              'sale.order.line': (
                                                  _get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'],
                                                  10),
                                          },
                                          multi='sums', help="The total amount after discount level 3 and without tax."),
    }

    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        cur_obj = self.pool.get('res.currency')
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_untaxed': 0.0,
                'amount_tax': 0.0,
                'amount_total': 0.0,
            }
            amount_tax = amount_untaxed = 0.0
            public_pricelist_discount_amount = 0.0
            amount_total_1_a=0.0
            order_line_discount_amount = 0.0
            amount_total_1 = amount_total_2 = amount_total_3 = 0.0
            cur = order.pricelist_id.currency_id
            sale_config = self.pool.get('ir.values')
            global_discount_limit = sale_config.get_default(cr, SUPERUSER_ID, 'sale.config.settings', 'global_amount_discount_limit')
            order_line_discount_limit = sale_config.get_default(cr, SUPERUSER_ID, 'sale.config.settings','order_line_amount_discount_limit')

            for line in order.order_line:
                if line.price_unit>0 :
                    amount_untaxed += line.price_subtotal
                    print amount_tax ," | ", amount_untaxed
                    amount_tax += self._amount_line_tax(cr, uid, line, context=context)
                    origin_price = line.product_id and line.product_id.list_price or 0
                    amount_total_1_a += (line.price_unit * line.product_uom_qty )

                    amount_total_1 += ( origin_price * line.product_uom_qty)


                    if origin_price > 0:
                        public_pricelist_discount_amount+= ( (origin_price - line.price_unit) * line.product_uom_qty )
                    if line.discount >0 :
                        order_line_discount_amount += ( (line.price_unit * line.product_uom_qty )- line.price_subtotal)

            discount_amount =0.0
            if order.discount_type == 'percent':
                discount_amount = amount_untaxed * order.discount_value / 100
            else:
                discount_amount = order.discount_value
            amount_total_2 = amount_untaxed
            amount_total_3 = amount_untaxed - discount_amount

            amount_untaxed = amount_untaxed - discount_amount
            amount_total = amount_untaxed + amount_tax

            total_order_line_discount = public_pricelist_discount_amount + order_line_discount_amount
            #print order_line_discount_limit , " | " , total_order_line_discount ," | ",order_line_discount_amount, " | ",public_pricelist_discount_amount

            if order_line_discount_limit and order_line_discount_limit>0 and total_order_line_discount > order_line_discount_limit:
                raise osv.except_osv(_('Error!'), _('Cannot create sales order, order line discount total must be lower than "%s" .') % \
                                            (str(order_line_discount_limit) ))

            if order.discount_type and global_discount_limit > 0 and discount_amount > global_discount_limit :
                raise osv.except_osv(_('Error!'), _(
                    'Cannot create sales order, global discount total must be lower than "%s" .') % \
                                     (str(global_discount_limit)))

            res[order.id]['amount_tax'] = cur_obj.round(cr, uid, cur, amount_tax)
            res[order.id]['amount_untaxed'] = cur_obj.round(cr, uid, cur, amount_untaxed)
            res[order.id]['discount_amount'] = cur_obj.round(cr, uid, cur, discount_amount)
            res[order.id]['amount_total'] = cur_obj.round(cr, uid, cur, amount_total)
            res[order.id]['public_pricelist_discount_amount'] = cur_obj.round(cr, uid, cur, public_pricelist_discount_amount)
            res[order.id]['order_line_discount_amount'] = cur_obj.round(cr, uid, cur, order_line_discount_amount)
            res[order.id]['amount_total_1'] = cur_obj.round(cr, uid, cur, amount_total_1)
            res[order.id]['amount_total_1_a'] = cur_obj.round(cr, uid, cur, amount_total_1_a)
            res[order.id]['amount_total_2'] = cur_obj.round(cr, uid, cur, amount_total_2)
            res[order.id]['amount_total_3'] = cur_obj.round(cr, uid, cur, amount_total_3)
        return res

    def _prepare_invoice(self, cr, uid, order, lines, context=None):

        res = super(sale_order, self)._prepare_invoice(cr, uid, order, lines,context);
        if res and order:
            res['amount_total_1'] = order.amount_total_1
            res['amount_total_1_a'] = order.amount_total_1_a
            res['amount_total_2'] = order.amount_total_2
            res['amount_total_3'] = order.amount_total_3
            res['discount_amount'] = order.discount_amount
            res['discount_value'] = order.discount_value
            res['public_pricelist_discount_amount'] = order.public_pricelist_discount_amount
            res['order_line_discount_amount'] = order.order_line_discount_amount

        return res




sale_order()


class sale_order_line(osv.osv):
    _inherit = 'sale.order.line'

    _columns = {
        'lst_price': fields.related('product_id', 'lst_price', type='float',  store=False, string='Public Price'),
    }


sale_order_line()

