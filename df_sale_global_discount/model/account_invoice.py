# from openerp.osv import fields, osv
# import openerp.addons.decimal_precision as dp
# from openerp.tools.translate import _
from openerp import models, fields, api, _
from samba.dcerpc.dns import res_rec
from openerp.tools import float_is_zero, float_compare
from openerp.exceptions import except_orm, Warning, RedirectWarning


class account_invoice(models.Model):
    _inherit = 'account.invoice'

    discount_type = fields.Selection(
        [('percent', 'Percentage'),
         ('amount', 'Amount')],
        string='Discount Type',)
    discount_value = fields.Float(string='Discount value', default='0.0', store=True)
    discount_amount = fields.Float(string='Global Discount Amount', default='0.0', store=True)
    public_pricelist_discount_amount = fields.Float(string='Total Public Pricelist Discount', default='0.0', store=True)
    order_line_discount_amount = fields.Float(string='Total Discount Order Line', default='0.0', store=True)

    amount_total_1 = fields.Float(string='Total Before Public Pricelist', default='0.0', store=True)
    amount_total_1_a = fields.Float(string='Total Unit Price Order Line', default='0.0', store=True)
    amount_total_2 = fields.Float(string='Total After Public Order Line Discount', default='0.0', store=True)
    amount_total_3 = fields.Float(string='Total After Public Global Discount', default='0.0', store=True)

    @api.one
    @api.depends('invoice_line.price_subtotal', 'tax_line.amount', 'currency_id', 'company_id')
    def _compute_amount(self):

        self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line)
        self.amount_tax = sum(line.amount for line in self.tax_line)
        discount_amount = amount_total = 0.0
        if self.discount_type == 'percent':
            discount_amount = self.amount_untaxed * self.discount_value / 100
        else:
            discount_amount = self.discount_value

        self.amount_untaxed = self.amount_untaxed - discount_amount
        amount_total = self.amount_untaxed + self.amount_tax
        self.amount_total = amount_total

        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed
        if self.currency_id and self.currency_id != self.company_id.currency_id:
            amount_total_company_signed = self.currency_id.compute(self.amount_total, self.company_id.currency_id)
            amount_untaxed_signed = self.currency_id.compute(self.amount_untaxed, self.company_id.currency_id)
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = self.amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign




    @api.one
    @api.depends(
        'state', 'currency_id', 'invoice_line.price_subtotal',
        'move_id.line_id.amount_residual',
        'move_id.line_id.currency_id')
    def _compute_residual(self):
        residual = 0.0
        residual_company_signed = 0.0
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        for line in self.sudo().move_id.line_id:
            if line.account_id.type in ('receivable', 'payable'):
                residual_company_signed += line.amount_residual
                if line.currency_id == self.currency_id:
                    residual += line.amount_residual_currency if line.currency_id else line.amount_residual
                else:
                    from_currency = (line.currency_id and line.currency_id.with_context(
                        date=line.date)) or line.company_id.currency_id.with_context(date=line.date)
                    residual += from_currency.compute(line.amount_residual, self.currency_id)
        total_discount = 0
        #print "------------------------- state : ",self.state
        #if self.state != 'draft':
        total_discount = self.discount_amount# + self.order_line_discount_amount + self.public_pricelist_discount_amount
        self.residual_company_signed = abs(residual_company_signed) * sign #- total_discount
        self.residual_signed = abs(residual) * sign #- total_discount
        self.residual = abs(residual) #- total_discount
        digits_rounding_precision = self.currency_id.rounding
        if float_is_zero(self.residual, precision_rounding=digits_rounding_precision):
            self.reconciled = True
        else:
            self.reconciled = False


    @api.multi
    def finalize_invoice_move_lines(self, move_lines):
        """ finalize_invoice_move_lines(move_lines) -> move_lines

            Hook method to be overridden in additional modules to verify and
            possibly alter the move lines to be created by an invoice, for
            special cases.
            :param move_lines: list of dictionaries with the account.move.lines (as for create())
            :return: the (possibly updated) final move_lines to create for this invoice
        """
        final_move_lines = super(account_invoice, self).finalize_invoice_move_lines(move_lines)

        total_discount = self.discount_amount +  self.order_line_discount_amount + self.public_pricelist_discount_amount
        if total_discount > 0 :
            date_maturity = False
            date = False
            partner_id = False

            sale_config = self.env['ir.values']
            discount_account_id = sale_config.get_default('sale.config.settings','discount_account_id');
            print "discount_account_id : ",discount_account_id
            if not discount_account_id :
                raise except_orm(_('Error!'), _(
                        'Cannot validate this invoice.Please choose account id for invoice who have sale discount.'))

            if final_move_lines:
                idx = 0
                public_price =0
                debit= 0
                credit=0
                for ml in final_move_lines:
                    date_maturity = ml[2]['date_maturity']
                    date = ml[2]['date']
                    partner_id = ml[2]['partner_id']
                    print ml[2]['product_id']," ",ml[2]['debit']," - ",ml[2]['credit']
                    if ml[2]['product_id']:
                        if ml[2]['credit'] > 0:
                            public_price += ( ml[2]['lst_price'] *  ml[2]['quantity'] )
                            final_move_lines[idx][2]['credit'] = public_price
                            credit += final_move_lines[idx][2]['credit']
                    elif ml[2]['debit'] > 0:
                        amount_to_pay =   ml[2]['debit'] - self.discount_amount
                        final_move_lines[idx][2]['debit'] = amount_to_pay
                        debit += final_move_lines[idx][2]['debit']
                    idx+=1

                diff = public_price  - debit
                print "d ",debit,"c ",credit," diff- > ",diff ," ",total_discount," <- Total discount"

                move_line = {
                    'analytic_account_id': False,
                    'tax_code_id': False,
                    'analytic_lines': [],
                    'tax_amount': False,
                    'name': 'Discount Total',
                    'ref': False,
                    'currency_id': False,
                    'credit': False,
                    'product_id': False,
                    'amount_currency': 0,
                    'product_uom_id': False,
                    'quantity': 1.0,
                    'date_maturity': date_maturity,
                    'debit': total_discount,
                    'date': date,
                    'partner_id': partner_id,
                    'account_id': discount_account_id
                }
                final_move_lines.append((final_move_lines[0][0],final_move_lines[0][1],move_line))

        print final_move_lines
        return final_move_lines
        #raise except_orm(_('Error!'), _(
        #        'You cannot cancel an invoice which is partially paid. You need to unreconcile related payment entries first.'))

    @api.model
    def line_get_convert(self, line, part, date):
        update_line = super(account_invoice, self).line_get_convert(line, part, date)
        update_line.update({'lst_price': line.get('lst_price', line['price'])})
        return update_line




class account_invoice_line(models.Model):
    _inherit = 'account.invoice.line'

    lst_price = fields.Float( string='Public Price',
                                 related='product_id.lst_price', store=False, readonly=True)

    @api.model
    def move_line_get_item(self, line):
        update_line = super(account_invoice_line, self).move_line_get_item(line)
        update_line.update({'lst_price':line.lst_price})
        return update_line

account_invoice_line()