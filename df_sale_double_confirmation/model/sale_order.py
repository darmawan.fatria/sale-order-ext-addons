from openerp import tools
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
import time
import logging
from openerp.tools.translate import _
from openerp import SUPERUSER_ID
from datetime import date, datetime, timedelta

class sale_order(osv.osv):
    _inherit = 'sale.order'

    # def create(self, cr, uid, vals, context=None):
    #     if context is None:
    #         context = {}
    #     if not self.can_override_back_date(cr,uid,context=None):
    #         date_order = vals.get('date_order', False)
    #         if date_order :
    #             today = date.today()
    #             diff = self.diff_month(today,datetime.strptime(date_order, '%Y-%m-%d %H:%M:%S'));
    #             if diff != 0:
    #                 raise osv.except_osv(_('Invalid Order!'), _('Date order must be within current period'))
    #     ctx = dict(context or {}, mail_create_nolog=True)
    #     return super(sale_order, self).create(cr, uid, vals, context=ctx);
    #
    # def write(self, cr, uid, ids, vals,context=None):
    #     if context is None:
    #         context = {}
    #     if not self.can_override_back_date(cr, uid, context=None):
    #         date_order = vals.get('date_order', False)
    #         if date_order:
    #             today = date.today()
    #             diff = self.diff_month(today, datetime.strptime(date_order, '%Y-%m-%d %H:%M:%S'));
    #             if diff != 0:
    #                 raise osv.except_osv(_('Invalid Order!'), _('Date order must be within current period'))
    #         else :
    #             for order in self.browse(cr,uid,ids,context=None):
    #                 today = date.today()
    #                 diff = self.diff_month(today, datetime.strptime(order.date_order, '%Y-%m-%d %H:%M:%S'));
    #                 if diff != 0:
    #                     raise osv.except_osv(_('Invalid Order!'), _('Date order must be within current period'))
    #
    #     ctx = dict(context or {}, mail_create_nolog=True)
    #     return super(sale_order, self).write(cr, uid, ids,vals, context=ctx);

    _columns = {
        'state': fields.selection([
            ('draft', 'Draft Quotation'),
            ('propose_sales_manager', 'Waiting Sales Manager Approval'),
            ('propose_finance_manager', 'Waiting Finance Manager Approval'),
            ('sent', 'Quotation Sent'),
            ('cancel', 'Cancelled'),
            ('waiting_date', 'Waiting Schedule'),
            ('progress', 'Sales Order'),
            ('manual', 'Sale to Invoice'),
            ('shipping_except', 'Shipping Exception'),
            ('invoice_except', 'Invoice Exception'),
            ('done', 'Done'),
            ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order.\
              \nThe exception status is automatically set when a cancel operation occurs \
              in the invoice validation (Invoice Exception) or in the picking list process (Shipping Exception).\nThe 'Waiting Schedule' status is set when the invoice is confirmed\
               but waiting for the scheduler to run on the order date.", select=True),
    }

    def action_propose_sales_manager(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        if not self.can_override_back_date(cr, uid, context=None):
            for order in self.browse(cr,uid,ids,context=None):
                today = date.today()
                diff = self.diff_month(today, datetime.strptime(order.date_order, '%Y-%m-%d %H:%M:%S'));
                if diff != 0:
                    raise osv.except_osv(_('Invalid Order!'), _('Date order must be within current period'))

        return self.write(cr, uid, ids, {'state': 'propose_sales_manager'})

    def can_override_back_date(self,cr,uid,context=None):
        group_pool = self.pool.get('res.groups')
        sale_config = self.pool.get('ir.values')
        override_back_date_role = sale_config.get_default(cr, SUPERUSER_ID, 'sale.config.settings', 'override_back_date_role')
        if override_back_date_role:
            group_obj = group_pool.browse(cr, SUPERUSER_ID, override_back_date_role, context=context)
            if group_obj and group_obj.users:
                for usr in group_obj.users:
                    if usr.id == uid:
                        return True
        return False

    def diff_month(self,d1, d2):
        return (d1.year - d2.year) * 12 + d1.month - d2.month



sale_order()
