from openerp import api, models, fields
from openerp.osv import fields, osv
from openerp import SUPERUSER_ID


class sale_config_settings(osv.TransientModel):
    _inherit = 'sale.config.settings'
    _columns = {
        'override_back_date_role': fields.many2one('res.groups', 'Override Back Date Role',),
    }

    def get_default_override_back_date_role(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        override_back_date_role = ir_values.get_default(cr, uid, 'sale.config.settings', 'override_back_date_role')
        return {
            'override_back_date_role': override_back_date_role,
        }

    def set_override_back_date_role(self, cr, uid, ids, context=None):
        ir_values = self.pool.get('ir.values')
        wizard = self.browse(cr, uid, ids)[0]
        if wizard.override_back_date_role:
            override_back_date_role = wizard.override_back_date_role.id
            ir_values.set_default(cr, SUPERUSER_ID, 'sale.config.settings', 'override_back_date_role', override_back_date_role)



