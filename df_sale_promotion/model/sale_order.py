from openerp import tools
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
import time
import logging
from openerp.tools.translate import _
from openerp import netsvc
from openerp import api
from datetime import date, datetime, timedelta

class sale_order(osv.osv):
    _inherit = 'sale.order'

    _columns = {
        'get_promotion': fields.boolean('Is Get Promotion ?'),
    }


    def action_calculate_promotion(self, cr, uid, ids,  context=None):
        print "calculate promotion...."

        promotion_pool = self.pool.get('sale.promotion')
        order_line_pool = self.pool.get('sale.order.line')

        #clear
        for order_id in ids :
            order_line_criteria = [('order_id', '=', order_id),
                                  ('free_product_promotion', '=', True)];
            order_line_ids = order_line_pool.search(cr, uid, order_line_criteria, context=None)
            order_line_pool.unlink(cr,uid,order_line_ids,context=None);
        for order in self.browse(cr,uid,ids,context=None):
            today = date.today()
            print today
            promotion_criteria = [('state', '=', 'confirm'),
                                  ('date_from', '<=', today),
                                  ('date_to', '>=', today)];
            promotion_ids = promotion_pool.search(cr, uid,promotion_criteria,context=None )
            print "promo : ",promotion_ids
            get_promotion = False
            current_promo = None
            if promotion_ids: #is any promotion ?
                for promo in promotion_pool.browse(cr, uid, promotion_ids, context=None):
                    print "type ",promo.type
                    promo_applied_to_partner = False
                    exist_partner = False
                    if promo.partner_ids:
                        print "partner_ids : ", promo.partner_ids
                        promo_applied_to_partner = True
                        for promo_by_partner in promo.partner_ids:
                            print "promo_by_partner : ", promo_by_partner
                            if promo_by_partner.id == order.partner_id.id:
                                exist_partner = True
                                break

                    print "is applied to partner : ",promo_applied_to_partner
                    if promo_applied_to_partner:
                        if not exist_partner:
                            break

                    if promo.type == 'product':
                        for line in order.order_line:
                            if line.product_id.id == promo.product_id.id and line.product_uom_qty >= promo.quantity:
                                get_promotion = True
                                for item_promo in promo.sale_promotion_item_ids:
                                    order_line_data = {}
                                    # tax = [(6, 0, vals['tax_id'])]
                                    tax = None
                                    qty = item_promo.quantity
                                    print line.product_uom_qty, ' / ',promo.quantity , ' = ', int(line.product_uom_qty/promo.quantity)
                                    if promo.allow_multipe :
                                        qty = ( int(line.product_uom_qty/promo.quantity) ) * item_promo.quantity
                                    print "qty : ", qty
                                    order_line_data.update({'product_id': item_promo.product_id.id,
                                                                'product_uom_qty': qty,
                                                                'order_id': order.id,
                                                                #'product_attributes': False,
                                                                'tax_id': tax,
                                                                'price_unit': 0.0,
                                                                'free_product_promotion': True
                                                                })
                                    order_line_pool.create(cr, uid, order_line_data, context=None);
                        if get_promotion:
                            break

                    elif promo.type == 'amount':
                        for item_promo in promo.sale_promotion_item_ids:
                            print "product : ", item_promo.product_id.name
                            if  order.amount_untaxed >= item_promo.amount:
                                get_promotion = True
                                order_line_data = {}
                                # tax = [(6, 0, vals['tax_id'])]
                                tax = None
                                qty = item_promo.quantity
                                print order.amount_untaxed, ' / ', item_promo.amount, ' = ', int(order.amount_untaxed / item_promo.amount)
                                if promo.allow_multipe:
                                    qty =  ( int(order.amount_untaxed / item_promo.amount) ) * item_promo.quantity
                                print "qty : ",qty
                                order_line_data.update({'product_id': item_promo.product_id.id,
                                                        'product_uom_qty': qty,
                                                        'order_id': order.id,
                                                        #'product_attributes': False,
                                                        'tax_id': tax,
                                                        'price_unit': 0.0,
                                                        'free_product_promotion': True
                                                        })
                                order_line_pool.create(cr, uid, order_line_data, context=None);
                            if get_promotion:
                                break

                print "get promo : ", get_promotion

        return True

sale_order()


class sale_order_line(osv.osv):
    _inherit = 'sale.order.line'

    _columns = {
        'free_product_promotion': fields.boolean('Product For Free'),
    }
sale_order_line()