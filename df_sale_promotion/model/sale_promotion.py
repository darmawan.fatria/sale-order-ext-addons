from openerp.osv import fields, osv
from datetime import date, datetime, timedelta
import time
import logging

_logger = logging.getLogger(__name__)


PROMOTION_STATES = [('draft', 'Draft'), ('open', 'Waiting For Approval'),('confirm', 'Confirm'),  ('reject', 'Reject'),  ('cancel', 'Cancel')]
class sale_promotion(osv.osv):
    _name = "sale.promotion"
    _description = 'Sale order promotion'

    _columns = {
        'name': fields.char('Promotion Name',size=120,required=True),
        'state': fields.selection(string='State', selection=PROMOTION_STATES, required=True, readonly=True),
        'date_from': fields.date('Period From',required=True),
        'date_to': fields.date('Period From', required=True),
        'type': fields.selection([
            ('product', 'By Product'),
            ('amount', 'By Amount')],
            "Type",
            required=True, readonly=True, states={'draft': [('readonly', False)]}),
        'product_id': fields.many2one('product.product', 'Product', help='', required=False, ),
        'quantity': fields.integer('Quantity', help='', required=False),
        'active' : fields.boolean('Active'),
        'allow_multipe': fields.boolean('Allow For Multipication ?'),
        'sale_promotion_item_ids': fields.one2many('sale.promotion.item', 'sale_promotion_id', readonly=True,
                                            states={'draft': [('readonly', False)],'open': [('readonly', False)]}),
        'partner_ids': fields.many2many('res.partner', 'sale_promotion_partner_rel', 'sale_promotion_id', 'partner_id', 'Customers', copy=False,domain=[('customer', '=', True)] ),
    }
    _defaults = {
        'name': '/',
        'state': 'draft',
        'date_from': time.strftime("%Y-%m-%d %H:%M:%S"),
        'active':True

    }



    def action_propose(self, cr, uid, ids, context=None):
        promotion_value = {'state': 'open'}
        self.write(cr, uid, ids, promotion_value, context=context)

    def action_confirm(self, cr, uid, ids, context=None):
        promotion_value = {'state': 'confirm'}
        self.write(cr, uid, ids, promotion_value, context=context)

    def action_reject(self, cr, uid, ids, context=None):
        promotion_value = {'state': 'reject'}
        self.write(cr, uid, ids, promotion_value, context=context)

    def action_cancel(self, cr, uid, ids, context=None):
        promotion_value = {'state': 'cancel',
                           'active': False}
        self.write(cr, uid, ids, promotion_value, context=context)

sale_promotion()

class sale_promotion_item(osv.osv):
    _name = "sale.promotion.item"
    _description = 'Sale order promotion item list'

    _columns = {
        'sale_promotion_id': fields.many2one('sale.promotion', 'Promotion'),
        'product_id': fields.many2one('product.product', 'Product', help='', required=True,),
        'quantity': fields.integer('Quantity', help='', required=True),
        'amount': fields.integer('Order Amount', help='if total order, will get free item', required=False),

    }

    _order = 'amount desc,quantity desc'

    def apply(self,cr,uid,ids,context=None):
        return True

sale_promotion_item()