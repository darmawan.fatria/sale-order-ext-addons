from openerp import tools
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
import time
import logging
from openerp.tools.translate import _
from openerp import netsvc
from openerp import api
from datetime import date, datetime, timedelta

class sale_order(osv.osv):
    _inherit = 'sale.order'

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        if vals.get('product_brand_id', False):
            product_brand_obj = self.pool.get('product.brand').browse(cr,uid,vals['product_brand_id'],context=None);
            if vals.get('name', '/') == '/' and product_brand_obj:
                vals['name'] = product_brand_obj.code +"/"+self.pool.get('ir.sequence').get(cr, uid, 'sale.order', context=context) or '/';

        ctx = dict(context or {}, mail_create_nolog=True)
        return super(sale_order, self).create(cr, uid, vals, context=ctx);

    _columns = {
        'product_brand_id': fields.many2one('product.brand', 'Product Brand',
                                            required=True,
                                            readonly=True, states={'draft': [('readonly', False)]}, ),
    }


sale_order()

class product_brand(osv.osv):
    _inherit = 'product.brand'


    _columns = {
        'code': fields.char('Code',size=5,required=True, )
    }


product_brand()

class stock_picking(osv.osv):
    _inherit = 'stock.picking'

    def create(self, cr, user, vals, context=None):
        context = context or {}
        if ('name' not in vals) or (vals.get('name') in ('/', False)):
            ptype_id = vals.get('picking_type_id', context.get('default_picking_type_id', False))
            sequence_id = self.pool.get('stock.picking.type').browse(cr, user, ptype_id, context=context).sequence_id.id
            vals['name'] = self.pool.get('ir.sequence').get_id(cr, user, sequence_id, 'id', context=context)
            if vals.get('origin', False):
                sale_pool = self.pool.get('sale.order')
                search_criteria = [('name', '=', vals['origin'])]
                sale_ids = sale_pool.search(cr, user, search_criteria, context=None)
                if sale_ids:
                    sale_order_obj = sale_pool.browse(cr, user, sale_ids,context=None);
                    if sale_order_obj:
                        vals['name'] = sale_order_obj[0].product_brand_id.code +"/"+vals['name']
        return super(stock_picking, self).create(cr, user, vals, context)



stock_picking()

class account_invoice(osv.osv):
    _inherit = 'account.invoice'

    def action_number(self, cr, uid, ids, context=None):
        super(account_invoice, self).action_number(cr, uid, ids, context)
        for inv in self.browse(cr,uid,ids,context=None):
            new_number = inv.number
            sale_pool = self.pool.get('sale.order')
            search_criteria = [('name', '=', inv.origin)]
            sale_ids = sale_pool.search(cr, uid, search_criteria, context=None)
            if sale_ids:
                sale_order_obj = sale_pool.browse(cr, uid, sale_ids, context=None);
                if sale_order_obj:
                    if sale_order_obj[0].product_brand_id:
                        new_number = sale_order_obj[0].product_brand_id.code + "/" + new_number


            self.write(cr,uid,[inv.id],{'internal_number': new_number,'number': new_number},context=None)
            if inv.type in ('in_invoice', 'in_refund'):
                if not inv.reference:
                    ref = new_number
                else:
                    ref = inv.reference
            else:
                ref = new_number

            cr.execute(""" UPDATE account_move SET ref=%s
                           WHERE id=%s AND (ref IS NULL OR ref = '')""",
                             (ref, inv.move_id.id))
            cr.execute(""" UPDATE account_move_line SET ref=%s
                           WHERE move_id=%s AND (ref IS NULL OR ref = '')""",
                             (ref, inv.move_id.id))
            cr.execute(""" UPDATE account_analytic_line SET ref=%s
                           FROM account_move_line
                           WHERE account_move_line.move_id = %s AND
                                 account_analytic_line.move_id = account_move_line.id""",
                             (ref, inv.move_id.id))
            #self.invalidate_cache();

        return True

account_invoice()